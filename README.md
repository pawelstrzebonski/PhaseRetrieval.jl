# PhaseRetrieval

## About

`PhaseRetrieval.jl` is a package for retrieving the near-field phase
given a pair of near-field and far-field intensity profiles.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/PhaseRetrieval.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for PhaseRetrieval.jl](https://pawelstrzebonski.gitlab.io/PhaseRetrieval.jl/).
