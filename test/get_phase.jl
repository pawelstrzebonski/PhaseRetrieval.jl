import PhaseRetrieval

dx = 0.1
x = -5:dx:5
nf = [exp(-(x^2)) for x in x]
nf ./= sum(nf)
ff = nf

# Simple test to see if any errors are thrown
@test (PhaseRetrieval.findphase(nf, ff); true)
@test (PhaseRetrieval.findphase(nf, ff, met = :gs); true)
@test (PhaseRetrieval.findphase(nf, ff, met = :hio); true)
@test (PhaseRetrieval.findphase(nf, ff, met = :sw); true)

dx = dy = 0.1
x = y = -5:dx:5
nf = [exp(-(x^2 + y^2)) for x in x, y in y]
nf ./= sum(nf)
ff = nf

# Simple test to see if any errors are thrown
@test (PhaseRetrieval.findphase(nf, ff); true)
@test (PhaseRetrieval.findphase(nf, ff, met = :gs); true)
@test (PhaseRetrieval.findphase(nf, ff, met = :hio); true)
@test (PhaseRetrieval.findphase(nf, ff, met = :sw); true)

# Test a bad method argument
@test_throws ErrorException PhaseRetrieval.findphase(nf, ff, met = :xxx)
