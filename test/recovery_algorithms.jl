import PhaseRetrieval

dx = dy = 0.1
x = y = -5:dx:5
nf = [exp(-(x^2 + y^2)) for x in x, y in y]
nf ./= sum(nf)
ff = nf

# Run basic tests to see if anything fails
# TODO: Add tests that verify output or error term performance

@test (PhaseRetrieval.gs(nf, ff, 100); true)
@test (PhaseRetrieval.hio(nf, ff, 0.1, 100); true)
@test (PhaseRetrieval.sw(nf, ff, 0.1, 100, 10); true)

dx = 0.1
x = -5:dx:5
nf = [exp(-(x^2)) for x in x]
nf ./= sum(nf)
ff = nf

# Run basic tests to see if anything fails
# TODO: Add tests that verify output or error term performance

@test (PhaseRetrieval.gs(nf, ff, 100); true)
@test (PhaseRetrieval.hio(nf, ff, 0.1, 100); true)
@test (PhaseRetrieval.sw(nf, ff, 0.1, 100, 10); true)
