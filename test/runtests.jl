import PhaseRetrieval
import Test: @test_broken, @test, @test_throws

tests = ["misc", "recovery_algorithms", "get_phase"]

#TODO: rtol set to account for 6 sigfig approximate numbers given
approxeq(a, b) = all(isapprox.(a, b, rtol = 1e-4))

aeq(a, b) = all(isapprox.(a, b, rtol = 1e-4))

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
