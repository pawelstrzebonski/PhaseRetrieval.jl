@test PhaseRetrieval.mean([1.5]) == 1.5
@test PhaseRetrieval.mean(collect(1:3)) == 2
@test PhaseRetrieval.mean([1 2; 3 4]) == 2.5
@test PhaseRetrieval.rms([1.5]) == 1.5
@test PhaseRetrieval.rms([-1.5]) == 1.5
@test PhaseRetrieval.rms(collect(1:3)) == sqrt(14 / 3)
@test PhaseRetrieval.rms([1 2; 3 4]) == sqrt(15 / 2)
@test PhaseRetrieval.errfun([1.5], [1.5]) == 0
@test aeq(PhaseRetrieval.errfun(collect(1:3), collect(2:4)), sqrt(2 / 3) * 5 / 90)
@test PhaseRetrieval.threshold(collect(1:5), 2) == [0, 2, 3, 4, 5]
@test PhaseRetrieval.threshold(collect(1:5), 2.1) == [0, 0, 3, 4, 5]

a = rand(10, 10)
@test aeq(abs.(a), PhaseRetrieval.xabs.(a))
@test aeq(PhaseRetrieval.cangle(1 + 1im), (1 + 1im) / sqrt(2))
@test aeq(PhaseRetrieval.cangle(0), 1)
@test aeq(PhaseRetrieval.cangle(0 + 0im), 1 + 0im)

@test length(PhaseRetrieval.gmat1(1, 1)) == 3
@test length(PhaseRetrieval.gmat1(1, 10)) == 21
@test approxeq(sum(PhaseRetrieval.gmat1(1, 10)), 1)
@test size(PhaseRetrieval.gmat2(1, 1)) == (3, 3)
@test size(PhaseRetrieval.gmat2(1, 10)) == (21, 21)
@test approxeq(sum(PhaseRetrieval.gmat2(1, 10)), 1)

x = ones(10)
@test size(PhaseRetrieval.gaussianfilter(x, 1)) == size(x)

x = ones(10, 10)
@test size(PhaseRetrieval.gaussianfilter(x, 1)) == size(x)

#TODO: magic_size_fields
#TODO: nf2ff
