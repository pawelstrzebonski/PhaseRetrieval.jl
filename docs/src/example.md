# Basic Usage

Let's consider a simple 1D phase retrieval example. First we import the
relevant packages and define the near-field and far-field images:

```@example ex1d
import PhaseRetrieval
import Plots: plot
import Plots: savefig # hide

dx=0.1
x=-5:dx:5
nf=[exp(-(x^2)) for x=x]
nf./=sum(nf)
ff=[exp(-(x^2))*abs(sin(x)) for x=x]
ff./=sum(ff)

plot(
	plot(x, nf),
	plot(x, ff),
	xlabel="x",
	ylabel="Field Intensity",
	legend=false,
)
savefig("example1d_fields.svg") # hide
```

![Example 1D fields](example1d_fields.svg)

Now that we've defined the near- and far-field intensities, we now
retrieve the near-field phase.

```@example ex1d
nf_phase=PhaseRetrieval.findphase(nf, ff)

plot(x, nf_phase,
	xlabel="x",
	ylabel="Phase",
	legend=false,
)
savefig("example1d_phase.svg") # hide
```

![Example 1D retrieved near-field phase](example1d_phase.svg)

While the phase retrieval function prints some error value, it is not
obvious from that value itself whether the returned phase is adequate
or if we need to run for additional iterations. So we propagate the
complex near-field and observe the resulting far-field (amplitude):

```@example ex1d
nearfield=@. nf*exp(im*nf_phase)
farfield=PhaseRetrieval.nf2ff(nearfield)

plot(x, [abs.(farfield), abs.(ff)],
	xlabel="x",
	ylabel="Amplitude",
	label=["Reconstructed" "Target"],
)
savefig("example1d_ff_amp.png") # hide
```

![Resulting far-field amplitude profile](example1d_ff_amp.png)

If this does not provide sufficient phase retrieval accuracy, we can
increase the number of iterations using the `maxit` argument. We can
also try a different algorithm using the `met` argument (see the
function documentation for a list of algorithms and explanation
of the options).

Now, we can do the same for 2D fields:

```@example ex2d
import PhaseRetrieval
import Plots: plot, heatmap
import Plots: savefig # hide

dx=dy=0.1
x=y=-5:dx:5
nf=[exp(-(x^2+y^2)) for x=x, y=y]
nf./=sum(nf)
ff=[exp(-(x^2+y^2))*abs(sin(x)) for x=x, y=y]
ff./=sum(ff)

plot(
	heatmap(x, y, nf', title="Near-Field"),
	heatmap(x, y, ff', title="Far-Field"),
	xlabel="x",
	ylabel="y",
	aspectratio=1,
)
savefig("example2d_fields.png") # hide
```

![Example 2D fields](example2d_fields.png)

Now that we've defined the near- and far-field intensities, we now
retrieve the near-field phase.

```@example ex2d
nf_phase=PhaseRetrieval.findphase(nf, ff)

heatmap(x, y, nf_phase',
	xlabel="x",
	ylabel="y",
	title="Near-Field Phase",
	aspectratio=1,
)
savefig("example2d_phase.png") # hide
```

![Example 2D retrieved near-field phase](example2d_phase.png)

While the phase retrieval function prints some error value, it is not
obvious from that value itself whether the returned phase is adequate
or if we need to run for additional iterations. So we propagate the
complex near-field and observe the resulting far-field (amplitude):

```@example ex2d
nearfield=@. nf*exp(im*nf_phase)
farfield=PhaseRetrieval.nf2ff(nearfield)

plot(
	heatmap(x, y, abs.(farfield'),
		xlabel="x",
		ylabel="y",
		title="Reconstructed Amplitude",
		aspectratio=1,
	),
	heatmap(x, y, abs.(ff'),
		xlabel="x",
		ylabel="y",
		title="Target Amplitude",
		aspectratio=1,
	)
)
savefig("example2d_ff_amp.png") # hide
```

![Resulting far-field amplitude profile](example2d_ff_amp.png)
