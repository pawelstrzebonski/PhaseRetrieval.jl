# recovery_algorithms.jl

Basic tests of the individual phase retrieval algorithms to check if it fails for various
expected inputs (including `JLArrays`, used as a basic test of `GPUArray`
friendliness).
