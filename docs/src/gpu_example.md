# GPU Usage

`PhaseRetrieval` should be written in a GPU friendly fashion, so that
(most of) it will work with the standard
[Julia GPU packages](https://juliagpu.org/).
All that should be needed is to convert the function input arrays
from being standard CPU arrays to some form of
[GPUArrays](https://github.com/JuliaGPU/GPUArrays.jl).
While GPU compute support tends to be somewhat spotty in terms of hardware,
drivers, and software, your best bet on using a GPU with `PhaseRetrieval`
is likely the
[CuArrays](https://github.com/JuliaGPU/CuArrays.jl)
package which support CUDA compatible NVIDIA GPUS. Assuming that
your near-field and far-field data `nf` and `ff` has been defined,
the following should be enough to retrieve the phase using your GPU:

```
import PhaseRetrieval
import CuArrays: cu
nf, ff=cu(nf), cu(ff)
phase=PhaseRetrieval.findphase(nf, ff)
```
