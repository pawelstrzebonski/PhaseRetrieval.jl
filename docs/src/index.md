# PhaseRetrieval.jl

The `PhaseRetrieval` package provides a simple way of phase retrieval
[Wikipedia](https://en.wikipedia.org/wiki/Phase_retrieval). Phase
retrieval is the problem of determining the near-field phase
given measurements of the near-field and corresponding far-field
amplitude. This package implements a few of the standard phase
retrieval algorithms (Gerchberg–Saxton, hybrid input-output, etc).

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/PhaseRetrieval.jl
```

## Features

* 1D and 2D support
* Gerchberg-Saxton and hybrid input-output algorithms implemented and working
* Shrinkwrap algorithm is a work-in-progress
* GPU compute friendly (Gerchberg-Saxton and hybrid input-output tested with `GPUArrays.JLArrays`)

## To Dos:

* Ensure shrinkwrap works correctly, is GPU compatible
* Document resources on algorithms
* Discuss limitations of algorithms

## Other Potentially Related/Useful Packages

* [DSP.jl](https://github.com/JuliaDSP/DSP.jl) for unwrapping the retrieved phase profiles
* [PRW.jl](https://gitlab.com/pawelstrzebonski/PRW.jl) for importing `PRW` files from certain goniometric radiometers (to obtain far-field measurements)
* [KNNRegression.jl](https://gitlab.com/pawelstrzebonski/KNNRegression.jl) for interpolation of spherical/polar grids from experimental far-fields onto Cartesian grids appropriate for this package
* [EMPropagation.jl](https://gitlab.com/pawelstrzebonski/EMPropagation.jl) for assorted field propagation functions (exploring how a complex near-field will propagate through space)
