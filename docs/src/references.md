# References

The basic concepts of phase retrieval and the basic algorithms implemented
in this package can be found on the
[Wikipedia article](https://en.wikipedia.org/wiki/Phase_retrieval)
or the references to that article.

TODO: Track down and list particularly good references
