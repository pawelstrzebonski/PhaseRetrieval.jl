# get_phase.jl

Basic tests of the `findphase` function to check if it fails for various
expected inputs (including `JLArrays`, used as a basic test of `GPUArray`
friendliness).
