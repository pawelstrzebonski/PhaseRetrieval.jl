using Documenter
import PhaseRetrieval

makedocs(
    sitename = "PhaseRetrieval.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "PhaseRetrieval.jl"),
    pages = [
        "Home" => "index.md",
        "Examples and Usage" =>
            ["Basic Usage" => "example.md", "GPU Usage" => "gpu_example.md"],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "get_phase.jl" => "get_phase.md",
            "misc.jl" => "misc.md",
            "recovery_algorithms.jl" => "recovery_algorithms.md",
        ],
        "test/" => [
            "get_phase.jl" => "get_phase_test.md",
            "misc.jl" => "misc_test.md",
            "recovery_algorithms.jl" => "recovery_algorithms_test.md",
        ],
    ],
)
