"""
    findphase(nf, ff[, met=:gs, beta=0.9, maxit=100, rnds=20])

Recover the near-field phase given the near-field and far-field intensity
measurements. Both near-field and far-fields should be of equally sized square fields.

# Arguments
- `nf::AbstractVecOrMat`: array of near-field magnitude (sqrt of power/intensity).
- `ff::AbstractVecOrMat`: array of far-field magnitude (sqrt of power/intensity).
- `met=:gs`: phase retrieval algorithm (`:sw`=Shrinkwrap, `:gs`=Gerchberg–Saxton, and `:hio`=Hybrid Input-Output).
- `beta::Number=0.9`: feedback parameter for hybrid input-output and shrinkwrap algorithms.
- `maxit::Integer=100`: iteration limit above which phase-retrieval will be finished.
- `rnds::Integer=20`: convolution-filtering iteration period for shrinkwrap algorithm.
"""
function findphase(
    nf::AbstractVecOrMat,
    ff::AbstractVecOrMat;
    met = :gs,
    beta::Number = 0.9,
    maxit::Integer = 100,
    rnds::Integer = 20,
)
    @assert size(nf) == size(ff) "Near-field and far-field are of different sizes!"
    @assert 0 <= beta && beta <= 1 "Beta must be between 0 and 1!"
    @assert isone(length(unique(size(nf)))) "Field measurements are not square in proportion!"
    @assert (all(nf .>= 0) && all(ff .>= 0)) "Field magnitudes are not all non-negative!"
    nf, ff = nf / sum(nf), ff / sum(ff)
    if met == :hio
        hio(nf, ff, beta, maxit)
    elseif met == :gs
        gs(nf, ff, maxit)
    elseif met == :sw
        sw(nf, ff, beta, maxit, rnds)
    else
        error("Unknown method specified (:hio, :gs, or :sw)")
    end
end
