import DSP

"""
    mean(x)

Arithmetic average of an array.
"""
mean(x::AbstractArray) = sum(x) / length(x[:])

"""
    rms(x)

Root-mean-square of an array.
"""
rms(x::AbstractArray) = sqrt(mean(x .^ 2))

"""
    errfun(x, y)

Error quantification function used by phase retrieval algorithms (RMS error).
"""
errfun(x::AbstractArray, y::AbstractArray) = rms((x ./ sum(x)) .- (y ./ sum(y)))

"""
    threshold(x, t)

Returns an array with all elements of value less than `t` zeroed.
"""
function threshold(x::AbstractArray, t::Number)
    (a -> a < t ? zero(a) : a).(x)
end

"""
    gmat1(sigma, radius)

Returns an array with Gaussian coefficients.
"""
function gmat1(sigma::Number, radius::Integer)
    g = [1 / (2 * pi * sigma^2) * exp(-(x^2) / sigma^2) for x = -radius:radius]
    g ./ sum(g)
end
"""
    gmat2(sigma, radius)

Returns an array with Gaussian coefficients.
"""
function gmat2(sigma::Number, radius::Integer)
    g = [
        1 / (2 * pi * sigma^2) * exp(-(x^2 + y^2) / sigma^2)
        for x = -radius:radius, y = -radius:radius
    ]
    g ./ sum(g)
end

"""
    gaussianfilter(x, sigma; radius=4)

Apply a Gaussian convolution filter.
"""
function gaussianfilter(x::AbstractVector, sigma::Number; radius::Integer = 4)
    DSP.conv(x, gmat1(sigma, radius))[(radius+1):(end-radius)]
end
function gaussianfilter(x::AbstractMatrix, sigma::Number; radius::Integer = 4)
    DSP.conv(x, gmat2(sigma, radius))[(radius+1):(end-radius), (radius+1):(end-radius)]
end

"""
    xabs(x)

An alternative implementation of `abs` using `sqrt` and `abs2` for possible
performance and GPU compatibility reasons.
"""
xabs(x) = sqrt(abs2(x))

"""
    cangle(x)

Returns a normalized complex number for `x`. If `x` is zero return `1`.
"""
cangle(x) = iszero(x) ? one(x) : x / xabs(x)

# Try to resize the nearfield and farfield to match array size and sampling
# TODO: Ensure this is truely reasonable, optimal
# TODO: Add this to the work-flow
"""
    magic_size_fields(dx1, nf, dt, ff, lambda)->(nf, ff)

Resize the near-field `nf` and farfield `ff` arrays so that the near-field
spatial sampling `dx1` and far-field angular sampling `dt` match and so
that both arrays have the same size. Requires the wavelength `lambda`.
"""
function magic_size_fields(
    dx1::AbstractArray{N},
    nf::AbstractArray{N},
    dt::AbstractArray{N},
    ff::AbstractArray{N},
    lambda::AbstractArray{N},
) where {N<:Number}
    # Based on the Computational Fourier Optics in Matlab book, we relate
    # far-field angular precision to near-field length:
    # DeltaX2=lambda*z/L1 => DeltaX2/z=lambda/L1~DeltaTheta
    # We try to determine nearfield size that gives the measured farfield
    # precision after FFT
    L1target = lambda / dt
    Nnf, Nff = length(nf), length(ff)
    Ntarget = round(Int, L1target / dx1)

    nf2, ff2 = zeros(Ntarget), zeros(Ntarget)
    if Nnf <= Ntarget
        # If the nearfield is smaller than the desired size, zeropad it
        nf2[1:length(nf)] .= nf
    else
        # If the nearfield is larger than desired, cut it down
        # First find bounds of non-zero elements
        l, r = findfirst(!iszero, nf), findlast(!iszero, nf)
        # Warn if we have to lose some power, and cut it off symmetrically
        if (r - l + 1) > Nnf
            @warn "Trimming down nearfield is lossy"
            dl = round(Int, (Nnf - (r - l + 1)) / 2)
            l, r = l + dl, r - dl
        end
        nf2 .= nf[(1:length(nf2)).+l.-1]
    end
    if Nff <= Ntarget
        # Ensure that the farfield is centered when zero-padded
        ff2[(1:Nff).+round(Int, (Ntarget - Nff) / 2, RoundDown)] .= ff
    else
        # If the farfield is larger than desired, cut it down
        # TODO: Determine lossiness of farfield cuts
        @warn "Trimming down farfield is likely lossy"
        dl = round(Int, (Nff - Ntarget) / 2)
        ff2 .= ff[(1:Ntarget).+dl]
    end
    nf2, ff2
end

"""
    nf2ff(nearfield)

Propagate a near-field into the far-field using FFT (note: the phase of the
far-field should not be trusted, use `EMPropagation` package for more
advanced near-field to far-field propagation).
"""
function nf2ff(nearfield::AbstractArray)
    nearfield = AbstractFFTs.fftshift(nearfield)
    farfield = AbstractFFTs.plan_fft!(nearfield) * nearfield
    farfield = AbstractFFTs.ifftshift(farfield)
end
