import AbstractFFTs

"""
    gs(nearfield::AbstractArray, farfield::AbstractArray, maxit::Integer)

Gerchberg–Saxton error reduction algorithm for phase retrieval.

# Arguments
- `nearfield`: near-field intensity array.
- `farfield`: far-field intensity array.
- `maxit`: iteration limit above which phase-retrieval will be finished.
"""
function gs(nearfield::AbstractArray, farfield::AbstractArray, maxit::Integer)
    @assert size(nearfield) == size(farfield)
    A = typeof(nearfield)
    # Shift the inputs to avoid shifting during fw and bw propagation
    farfield = AbstractFFTs.ifftshift(farfield)
    nearfield = AbstractFFTs.fftshift(nearfield)
    # Assign nf to have random phase
    nf = A(rand(-1:0.1:1, size(nearfield))) + im * A(rand(-1:0.1:1, size(nearfield)))
    nf = @. nearfield * cangle(nf)
    # Use fft plans for better performance when doing lots of FFTs
    fw_fft_plan = AbstractFFTs.plan_fft!(nf)
    bw_ifft_plan = AbstractFFTs.plan_ifft!(nf)
    ff = fw_fft_plan * nf
    tmp = similar(ff)
    tmp .= ff
    for _ = 1:maxit
        #nf=bw_ifft_plan*(farfield.*cangle.(ff))
        tmp .= cangle.(tmp)
        tmp .*= farfield
        bw_ifft_plan * tmp
        #ff=fw_fft_plan*(nearfield.*cangle.(nf))
        tmp .= cangle.(tmp)
        tmp .*= nearfield
        fw_fft_plan * tmp
    end
    ff .= tmp
    tmp .= cangle.(tmp)
    tmp .*= farfield
    bw_ifft_plan * tmp
    nf .= tmp
    @info string(
        "Finished after ",
        maxit,
        " iterations with error of ",
        errfun(farfield, xabs.(ff)),
    )
    # Since we earlier shifted everything, we now unshift for output
    angle.(AbstractFFTs.ifftshift(nf))
end

"""
    hio(nearfield::AbstractArray, farfield::AbstractArray, Beta::Number, maxit::Integer)

Hybrid Input-Output algorithm for phase retrieval.

# Arguments
- `nearfield`: near-field intensity array.
- `farfield`: far-field intensity array.
- `Beta`: feedback parameter.
- `maxit`: iteration limit above which phase-retrieval will be finished.
"""
function hio(
    nearfield::AbstractArray,
    farfield::AbstractArray,
    Beta::Number,
    maxit::Integer,
)
    @assert size(nearfield) == size(farfield)
    A = typeof(nearfield)
    # Shift the inputs to avoid shifting during fw and bw propagation
    farfield = AbstractFFTs.ifftshift(farfield)
    nearfield = AbstractFFTs.fftshift(nearfield)
    # Assign nf to have random phase
    nf = A(rand(-1:0.1:1, size(nearfield))) + im * A(rand(-1:0.1:1, size(nearfield)))
    nf = @. nearfield * cangle(nf)
    # Use fft plans for better performance when doing lots of FFTs
    fw_fft_plan = AbstractFFTs.plan_fft!(nf)
    bw_ifft_plan = AbstractFFTs.plan_ifft!(nf)
    ff = fw_fft_plan * nf
    tmp = similar(ff)
    tmpp = similar(ff)
    tmp .= ff
    for _ = 1:maxit
        #nfp=bw_ifft_plan*(farfield.*cangle.(ff))
        tmpp .= cangle.(tmp)
        tmpp .*= farfield
        bw_ifft_plan * tmpp
        #nf=@. Beta*cangle(nfp)+(1-Beta)*cangle(nf)
        tmpp .= cangle.(tmpp)
        tmpp .*= Beta
        tmp .= cangle.(tmp)
        tmp .*= 1 - Beta
        tmp .+= tmpp
        #ff=fw_fft_plan*(nearfield.*cangle.(nf))
        tmp .= cangle.(tmp)
        tmp .*= nearfield
        fw_fft_plan * tmp
    end
    ff .= tmp
    tmp .= cangle.(tmp)
    tmp .*= farfield
    bw_ifft_plan * tmp
    nf .= tmp
    @info string(
        "Finished after ",
        maxit,
        " iterations with error of ",
        errfun(farfield, xabs.(ff)),
    )
    angle.(AbstractFFTs.ifftshift(nf))
end

#TODO: sw() is not yet properly working? Fix this.
#https://arxiv.org/abs/physics/0306174
"""
    sw(nearfield::AbstractArray, farfield::AbstractArray, Beta::Number, maxit::Integer, rnds::Integer)

Shrinkwrap algorithm for phase retrieval.

# Arguments
- `nearfield`: near-field intensity array.
- `farfield`: far-field intensity array.
- `Beta`: feedback parameter.
- `maxit`: iteration limit above which phase-retrieval will be finished.
- `rnds`: convolution-filtering iteration period
"""
function sw(
    nearfield::AbstractArray,
    farfield::AbstractArray,
    Beta::Number,
    maxit::Integer,
    rnds::Integer,
)
    @warn "Work in progress. May not be fully functional."
    @assert size(nearfield) == size(farfield)
    A = typeof(nearfield)
    # Shift the inputs to avoid shifting during fw and bw propagation
    farfield = AbstractFFTs.ifftshift(farfield)
    nearfield = AbstractFFTs.fftshift(nearfield)
    # Assign nf to have random phase
    nf = A(rand(-1:0.1:1, size(nearfield))) .+ im * A(rand(-1:0.1:1, size(nearfield)))
    nf = @. nearfield * cangle(nf)
    fw_fft_plan = AbstractFFTs.plan_fft!(nf)
    bw_ifft_plan = AbstractFFTs.plan_ifft!(nf)
    ff = fw_fft_plan * nf
    tmp = similar(ff)
    tmpp = similar(ff)
    tmp .= ff
    for i = 1:maxit
        #nfp=bw_ifft_plan*(farfield.*cangle.(ff))
        tmpp .= cangle.(tmp)
        if iszero(i % rnds)
            # TODO: filtering on a shifted nearfield? use wrapping convolution?
            tmpp .*= threshold(
                gaussianfilter(farfield, clamp(3 * 0.99^(i / rnds), 1.5, 3)),
                0.2 * maximum(farfield),
            )
        else
            tmpp .*= farfield
        end
        bw_ifft_plan * tmpp
        #nf=@. Beta*cangle(nfp)+(1-Beta)*cangle(nf)
        tmpp .= cangle.(tmpp)
        tmpp .*= Beta
        tmp .= cangle.(tmp)
        tmp .*= 1 - Beta
        tmp .+= tmpp
        #ff=fw_fft_plan*(nearfield.*cangle.(nf))
        tmp .= cangle.(tmp)
        tmp .*= nearfield
        fw_fft_plan * tmp
    end
    ff .= tmp
    tmp .= cangle.(tmp)
    tmp .*= farfield
    bw_ifft_plan * tmp
    nf .= tmp
    @info string(
        "Finished after ",
        maxit,
        " iterations with error of ",
        errfun(farfield, xabs.(ff)),
    )
    angle.(AbstractFFTs.ifftshift(nf))
end
